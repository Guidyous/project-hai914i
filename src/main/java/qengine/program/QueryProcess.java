package qengine.program;

import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.query.algebra.StatementPattern;
import org.eclipse.rdf4j.query.algebra.helpers.StatementPatternCollector;
import org.eclipse.rdf4j.query.parser.ParsedQuery;
import org.eclipse.rdf4j.query.parser.sparql.SPARQLParser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QueryProcess {
    private final List<List<String>> queryResults;
    private final String queryFile;
    private final MainRDFHandler mainRDFHandler;
    private final List<ParsedQuery> queryList;

    public QueryProcess(String queryFile, MainRDFHandler mainRDFHandler) {
        this.queryFile = queryFile;
        this.mainRDFHandler = mainRDFHandler;
        this.queryResults = new ArrayList<>();
        this.queryList = new ArrayList<>();
    }

    // Getters

    public List<List<String>> getQueryResults() {
        return this.queryResults;
    }

    public List<ParsedQuery> getQueryList() {
        return this.queryList;
    }

    // Methods

    public void processQueries() {
        for (ParsedQuery query : this.queryList) {
            this.queryResults.add(processQuery(query));
        }
    }

    private List<String> processQuery(ParsedQuery query) {
        List<StatementPattern> patterns = StatementPatternCollector.process(query.getTupleExpr());

        List<Long> statementResults = null;

        for (StatementPattern statement : patterns) {

            Value predicate = statement.getPredicateVar().getValue();
            Value object = statement.getObjectVar().getValue();


            long predicateId = this.mainRDFHandler.getDictionary().getIdFromKey(predicate.toString());
            long objectId = this.mainRDFHandler.getDictionary().getIdFromKey(object.toString());
            List<Long> resultOfQuery = this.mainRDFHandler.getIndexTreeMapByNameIndex("pos").get(predicateId, objectId);

            if (statementResults == null) {
                statementResults = new ArrayList<>(resultOfQuery);
            } else {
                statementResults.retainAll(resultOfQuery);
            }
        }

        if (statementResults == null) {
            statementResults = Collections.emptyList();
        }

        List<String> resultList = new ArrayList<>();
        for (Long id : statementResults) {
            resultList.add(this.mainRDFHandler.getDictionary().getKeyFromId(id));
        }
        return resultList;
    }

    public void parseQueries() throws IOException {
        SPARQLParser sparqlParser = new SPARQLParser();
        List<String> lines = new ArrayList<>(Files.readAllLines(Paths.get(queryFile)));

        StringBuilder queryString = new StringBuilder();

        for (String line : lines) {
            queryString.append(line);
            if (line.trim().contains("}")) {
                long startQueryReadTime = System.nanoTime();

                ParsedQuery query = sparqlParser.parseQuery(queryString.toString(), null);

                EvaluationMetrics.queryReadTime += System.nanoTime() - startQueryReadTime;

                this.queryList.add(query);
                queryString.setLength(0);
            }
        }
    }

    public void shuffleQueries() {
        Collections.shuffle(this.queryList);
    }

    public void warmQueryProcess(int warmPercentage) {
        List<ParsedQuery> parsedQueryList = new ArrayList<>(this.queryList);
        Collections.shuffle(parsedQueryList);
        long percentageOfSizeOfParsedQueryList = Math.round((parsedQueryList.size() * warmPercentage) / 100.);

        for (int i = 0; i < percentageOfSizeOfParsedQueryList; i++) {
            processQuery(parsedQueryList.get(i));
        }
    }
}
