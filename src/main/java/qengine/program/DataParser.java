package qengine.program;

import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParser;
import org.eclipse.rdf4j.rio.Rio;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class DataParser {

    public static void parseData(String dataFile, MainRDFHandler mainRdfHandler) throws IOException {

        try (Reader dataReader = new FileReader(dataFile)) {
            // On va parser des données au format ntriples
            RDFParser rdfParser = Rio.createParser(RDFFormat.NTRIPLES);

            // On utilise notre implémentation de handler
            rdfParser.setRDFHandler(mainRdfHandler);

            // Parsing et traitement de chaque triple par le handler
            rdfParser.parse(dataReader, null);
        }
    }
}
