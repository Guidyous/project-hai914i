package qengine.program;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Dictionary {
    private final Map<String, Long> dictionaryStringLongMap;
    private final List<String> dictionaryStringList;

    private long nextId;

    public Dictionary() {
        this.dictionaryStringLongMap = new HashMap<>();
        this.dictionaryStringList = new ArrayList<>();
        this.nextId = 1;
    }

    public void insert(String key) {
        if (this.dictionaryStringLongMap.putIfAbsent(key, this.nextId) == null) {
            this.dictionaryStringList.add(key);
            this.nextId++;
            EvaluationMetrics.indexCount++;
        }
    }

    public long getIdFromKey(String key) {
        return this.dictionaryStringLongMap.getOrDefault(key, 0L);
    }

    public String getKeyFromId(long id) {
        return this.dictionaryStringList.get((int) id - 1);
    }
}
