package qengine.program;

import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RDFDataMgr;

import java.util.ArrayList;
import java.util.List;

public class Jena {
    Model model;
    List<String> queryList;

    List<List<String>> resultList;

    public Jena(String dataPath, List<String> queryList) {
        model = ModelFactory.createDefaultModel();
        ARQ.init();

        try {
            RDFDataMgr.read(model, dataPath);
        } catch (Exception e) {
            System.err.println("Erreur lors de la lecture du fichier: " + e.getMessage());
        }
        this.queryList = queryList;
        resultList = new ArrayList<>();
    }

    public void executeQueries() {
        for (String query : this.queryList) {
            Query jenaQuery = QueryFactory.create(query);
            try (QueryExecution qexec = QueryExecutionFactory.create(jenaQuery, this.model)) {
                if (jenaQuery.isSelectType()) {
                    ResultSet results = qexec.execSelect();
                    List<String> resultList = new ArrayList<>();
                    while (results.hasNext()) {
                        resultList.add(results.nextSolution().get("v0").toString());
                    }

                    this.resultList.add(resultList);
                }

            }
        }
    }
}
