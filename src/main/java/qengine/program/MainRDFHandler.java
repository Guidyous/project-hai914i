package qengine.program;

import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.rio.helpers.AbstractRDFHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * Le RDFHandler intervient lors du parsing de données et permet d'appliquer un traitement pour chaque élément lu par le parseur.
 *
 * <p>
 * Ce qui servira surtout dans le programme est la méthode {@link #handleStatement(Statement)} qui va permettre de traiter chaque triple lu.
 * </p>
 * <p>
 * À adapter/réécrire selon vos traitements.
 * </p>
 */
public final class MainRDFHandler extends AbstractRDFHandler {
    private final Dictionary dictionary;

    private final Map<String, IndexTree> indexTreeMap;

    public MainRDFHandler() {
        this.dictionary = new Dictionary();
        this.indexTreeMap = new HashMap<>(Map.of("spo",
                                                 new IndexTree(),
                                                 "sop",
                                                 new IndexTree(),
                                                 "pos",
                                                 new IndexTree(),
                                                 "pso",
                                                 new IndexTree(),
                                                 "ops",
                                                 new IndexTree(),
                                                 "osp",
                                                 new IndexTree()));
    }

    public Dictionary getDictionary() {
        return dictionary;
    }

    public IndexTree getIndexTreeMapByNameIndex(String nameIndex) {
        return indexTreeMap.get(nameIndex);
    }

    private void updateIndexes(long subject,
                               long predicate,
                               long object) {
        this.indexTreeMap.get("spo").insert(subject, predicate, object);
        this.indexTreeMap.get("sop").insert(subject, object, predicate);
        this.indexTreeMap.get("pos").insert(predicate, object, subject);
        this.indexTreeMap.get("pso").insert(predicate, subject, object);
        this.indexTreeMap.get("ops").insert(object, predicate, subject);
        this.indexTreeMap.get("osp").insert(object, subject, predicate);
    }

    @Override
    public void handleStatement(Statement st) {
        EvaluationMetrics.rdfTripletsCount++;

        Long startTime = System.nanoTime();
        this.dictionary.insert(st.getSubject().toString());
        this.dictionary.insert(st.getPredicate().toString());
        this.dictionary.insert(st.getObject().toString());
        Long intermediateTime = System.nanoTime();
        this.updateIndexes(this.dictionary.getIdFromKey(st.getSubject().toString()),
                           this.dictionary.getIdFromKey(st.getPredicate().toString()),
                           this.dictionary.getIdFromKey(st.getObject().toString()));
        Long endTime = System.nanoTime();
        EvaluationMetrics.dictionaryCreationTime += intermediateTime - startTime;
        EvaluationMetrics.indexCreationTime += endTime - intermediateTime;
    }
}