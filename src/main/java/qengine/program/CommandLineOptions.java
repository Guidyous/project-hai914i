package qengine.program;

import org.apache.commons.cli.*;

import static java.lang.System.exit;

public class CommandLineOptions {
    // Définition des champs
    private String queriesPath;
    private String dataPath;
    private String outputPath;
    private boolean useJena = false;
    private int warmPercentage = 0;
    private boolean shuffle = false;

    public String getQueriesPath() {
        return queriesPath;
    }

    public String getDataPath() {
        return dataPath;
    }

    public String getOutputPath() {
        return outputPath;
    }

    public boolean isUseJena() {
        return useJena;
    }

    public int getWarmPercentage() {
        return warmPercentage;
    }

    public boolean isShuffle() {
        return shuffle;
    }

    public void parseArguments(String[] args) {
        // Définition des options
        Options options = new Options();

        options.addOption(Option.builder("queries")
                                  .argName("queriesPath")
                                  .hasArg()
                                  .desc("Chemin vers le dossier des requêtes (obligatoire)")
                                  .build());

        options.addOption(Option.builder("data")
                                  .argName("dataPath")
                                  .hasArg()
                                  .desc("Chemin vers le fichier de données (obligatoire)")
                                  .build());

        options.addOption(Option.builder("output")
                                  .argName("outputPath")
                                  .hasArg()
                                  .desc("Chemin vers le dossier de sortie (obligatoire)")
                                  .build());

        options.addOption(new Option("Jena",
                                     "Active la vérification avec Jena"));

        options.addOption(Option.builder("warm")
                                  .argName("warmPercentage")
                                  .hasArg()
                                  .desc("Pourcentage pour chauffer le système")
                                  .build());

        options.addOption(new Option("shuffle",
                                     "Considère une permutation aléatoire des requêtes"));

        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options,
                                           args);

            if (cmd.hasOption("queries") && cmd.hasOption("data") && cmd.hasOption("output")) {
                this.queriesPath = cmd.getOptionValue("queries");
                this.dataPath = cmd.getOptionValue("data");
                this.outputPath = cmd.getOptionValue("output");
            } else {
                printHelp(options);
                exit(1);
            }

            if (cmd.hasOption("warm")) {
                this.warmPercentage = Integer.parseInt(cmd.getOptionValue("warm"));
            }

            this.useJena = cmd.hasOption("Jena");

            this.shuffle = cmd.hasOption("shuffle");

            if (args.length == 0 || args[0].equals("--help")) {
                printHelp(options);
            }
        } catch (ParseException e) {
            System.err.println("Erreur de parsing des arguments: " + e.getMessage());
            // Vous pouvez également afficher automatiquement l'aide ici.
        }
    }

    private void printHelp(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        String header = "Options pour RDF Engine:";
        String footer = "\nPour plus d'informations, contactez l'administrateur.";
        formatter.printHelp("java -jar rdfengine.jar",
                            header,
                            options,
                            footer,
                            true);
    }
}

