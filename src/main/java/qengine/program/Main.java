package qengine.program;

import org.eclipse.rdf4j.query.parser.ParsedQuery;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Programme simple lisant un fichier de requête et un fichier de données.
 *
 * <p>
 * Les entrées sont données ici de manière statique,
 * à vous de programmer les entrées par passage d'arguments en ligne de commande comme demandé dans l'énoncé.
 * </p>
 *
 * <p>
 * Le présent programme se contente de vous montrer la voie pour lire les triples et requêtes
 * depuis les fichiers ; ce sera à vous d'adapter/réécrire le code pour finalement utiliser les requêtes et interroger les données.
 * On ne s'attend pas forcémment à ce que vous gardiez la même structure de code, vous pouvez tout réécrire.
 * </p>
 *
 * @author Olivier Rodriguez <olivier.rodriguez1@umontpellier.fr>
 */
final class Main {
    public static void main(String[] args) throws Exception {
        long startProgramTime = System.nanoTime();
        CommandLineOptions options = new CommandLineOptions();
        options.parseArguments(args);

        String queryFile = options.getQueriesPath();
        String dataFile = options.getDataPath();
        String output = options.getOutputPath();
        final MainRDFHandler mainRdfHandler = new MainRDFHandler();
        final QueryProcess queryProcess = new QueryProcess(queryFile,
                                                           mainRdfHandler);

        EvaluationMetrics.dataFileName = dataFile;
        EvaluationMetrics.queryFolderName = queryFile;

        Long startDataReadTime = System.nanoTime();
        DataParser.parseData(dataFile,
                             mainRdfHandler);
        Long endDataReadTime = System.nanoTime();

        queryProcess.parseQueries();
        if (options.isShuffle()) {
            queryProcess.shuffleQueries();
        }
        if (options.getWarmPercentage() != 0) {
            queryProcess.warmQueryProcess(options.getWarmPercentage());
        }
        Long startWorkloadTime = System.nanoTime();
        queryProcess.processQueries();
        Long endWorkloadTime = System.nanoTime();

        EvaluationMetrics.queryCount = queryProcess.getQueryList()
                .size();
        EvaluationMetrics.dataReadTime = endDataReadTime - startDataReadTime;
        EvaluationMetrics.workloadEvaluationTime = endWorkloadTime - startWorkloadTime;

        List<String> queryList = queryProcess.getQueryList()
                .stream()
                .map(ParsedQuery::getSourceString)
                .collect(Collectors.toList());
        if (options.isUseJena()) {
            Jena jena = new Jena(dataFile,
                                 queryList);
            long startWorkloadJenaTime = System.nanoTime();
            jena.executeQueries();
            EvaluationMetrics.jenaTime = System.nanoTime() - startWorkloadJenaTime;
            EvaluationMetrics.exportCompareResultModelRDF(output,
                                                          queryList,
                                                          queryProcess.getQueryResults(),
                                                          jena.resultList);
        }

        EvaluationMetrics.totalTime = System.nanoTime() - startProgramTime;

        EvaluationMetrics.exportMetricsToCSV(output);
        EvaluationMetrics.exportResultQueries(output,
                                              queryProcess.getQueryResults());
    }


}
