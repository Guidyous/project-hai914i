package qengine.program;

import java.util.*;

public class IndexTree {
    Map<Long, Map<Long, List<Long>>> indexTree;

    public IndexTree() {
        this.indexTree = new HashMap<>();
    }

    public void insert(long first, long second, long last) {
        this.indexTree.computeIfAbsent(first, k -> new HashMap<>())
                      .computeIfAbsent(second, k -> new ArrayList<>())
                      .add(last);
    }

    public List<Long> get(long first, long second) {
        return this.indexTree.getOrDefault(first, Collections.emptyMap())
                             .getOrDefault(second, Collections.emptyList());
    }
}
