package qengine.program;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class EvaluationMetrics {
    static String dataFileName = "";
    static String queryFolderName = "";
    static int rdfTripletsCount = 0;
    static int queryCount = 0;
    static double dataReadTime = 0.;
    static double queryReadTime = 0.;
    static double dictionaryCreationTime = 0.;
    static int indexCount = 0;
    static double indexCreationTime = 0.;
    static double workloadEvaluationTime = 0.;
    static double totalTime = 0.;
    static double jenaTime = 0.;

    private static List<String> toCsvString() {
        return new ArrayList<>(List.of(
                dataFileName,
                queryFolderName,
                String.valueOf(rdfTripletsCount),
                String.valueOf(queryCount),
                formatTime(roundTimeInSec(dataReadTime)),
                formatTime(roundTimeInSec(queryReadTime)),
                formatTime(roundTimeInSec(dictionaryCreationTime)),
                String.valueOf(indexCount),
                formatTime(roundTimeInSec(indexCreationTime)),
                formatTime(roundTimeInSec(workloadEvaluationTime)),
                formatTime(roundTimeInSec(jenaTime)),
                formatTime(roundTimeInSec(totalTime))));
    }

    private static String formatTime(double time) {
        return time != 0 ? String.valueOf(time) : "NON_DISPONIBLE";
    }

    public static void exportMetricsToCSV(String outputPath) {
        String csvFileName = outputPath + File.separator + "evaluation_metrics.csv";

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(csvFileName,
                                                                       StandardCharsets.UTF_8));
             CSVPrinter csvPrinter = new CSVPrinter(writer,
                                                    CSVFormat.DEFAULT.builder()
                                                            .setHeader("Nom du fichier de données",
                                                                       "Nom du dossier des requêtes",
                                                                       "Nombre de triplets RDF",
                                                                       "Nombre de requêtes",
                                                                       "Temps de lecture des données (s)",
                                                                       "Temps de lecture des requêtes (s)",
                                                                       "Temps création dico (s)",
                                                                       "Nombre d’index",
                                                                       "Temps de création des index (s)",
                                                                       "Temps total d’évaluation du workload (s)",
                                                                       "Jena execution (s)",
                                                                       "Temps total (du début à la fin du programme) (s)")
                                                            .build()
             )) {

            csvPrinter.printRecord(toCsvString());
            csvPrinter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static double roundTimeInSec(double time) {
        double timeInSecondes = time / 1000000000.;
        return Math.round(timeInSecondes * 1000.) / 1000.;
    }

    private static boolean compareResultModelRDF(List<String> ourResultModel, List<String> jenaResultModel) {
        return ourResultModel.containsAll((jenaResultModel));
    }

    public static void exportCompareResultModelRDF(String outputPath, List<String> queryList,
                                                   List<List<String>> ourResultModel,
                                                   List<List<String>> jenaResultModel) {
        String csvFileName = outputPath + File.separator + "compareResultModelRDF.csv";

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(csvFileName,
                                                                       StandardCharsets.UTF_8));
             CSVPrinter csvPrinter = new CSVPrinter(writer,
                                                    CSVFormat.DEFAULT.builder()
                                                            .setHeader("Query",
                                                                       "Comparison Result")
                                                            .build()
             )) {

            for (int i = 0; i < queryList.size(); i++) {
                String query = queryList.get(i);
                boolean comparisonResult = compareResultModelRDF(ourResultModel.get(i),
                                                                 jenaResultModel.get(i));
                csvPrinter.printRecord(query,
                                       comparisonResult);
            }

            csvPrinter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void exportResultQueries(String outputPath, List<List<String>> queryResults) {
        String csvFileName = outputPath + File.separator + "queries_results.csv";

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(csvFileName,
                                                                       StandardCharsets.UTF_8));
             CSVPrinter csvPrinter = new CSVPrinter(writer,
                                                    CSVFormat.DEFAULT)) {

            for (List<String> queryResult : queryResults) {
                if (queryResult.isEmpty()) {
                    csvPrinter.printRecord((Object) null);
                } else {
                    csvPrinter.printRecord(String.join(",",
                                                       queryResult));
                }
            }

            csvPrinter.flush();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}

