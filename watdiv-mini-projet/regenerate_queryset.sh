#!/bin/bash

# Obtention de la date et l'heure actuelles au format "YYYYMMDD_HHMMSS"
current_time=$(date +"%Y%m%d_%H%M%S")

# Nom du fichier de sortie pour toutes les requêtes
output_file="testsuite/queries/all_queries_$current_time.queryset"

# Initialisation du fichier de sortie
echo "" > "$output_file"

# Génération des requêtes et ajout dans le fichier de sortie unique
for qt in testsuite/templates/*.sparql-template; do
   bin/Release/watdiv -q model/wsdbm-data-model.txt "${qt}" 100 1 >> "$output_file"
done

echo "Toutes les requêtes ont été générées et stockées dans le fichier: $output_file"